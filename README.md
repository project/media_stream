# Media Stream

The module enables HTML5 audio and video formatters for link fields on Media entities.

## Links
Project page: https://www.drupal.org/project/media_stream
Relevant Drupal core issue: https://www.drupal.org/node/2927166

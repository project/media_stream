<?php

declare(strict_types=1);

namespace Drupal\Tests\media_stream\FunctionalJavascript;

use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\file\Entity\File;
use Drupal\Tests\media\FunctionalJavascript\MediaSourceTestBase;

/**
 * Tests the Audio and Video media sources.
 *
 * @group media
 */
class MediaStreamSourceAudioVideoTest extends MediaSourceTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'media_stream',
  ];

  /**
   * Check the Audio Stream source + formatter functionality.
   */
  public function testAudioStreamTypeCreation() {
    $assert_session = $this->assertSession();
    $page = $this->getSession()->getPage();

    $source_id = 'audio_stream';
    $type_name = 'audio_stream_type';
    $field_name = 'field_media_' . $source_id;
    $this->doTestCreateMediaType($type_name, $source_id);

    // Check that the source field was created with the correct settings.
    $storage = FieldStorageConfig::load("media.$field_name");
    $this->assertInstanceOf(FieldStorageConfig::class, $storage);
    $field = FieldConfig::load("media.$type_name.$field_name");
    $this->assertInstanceOf(FieldConfig::class, $field);

    // Check that the display holds the correct formatter configuration.
    $display = EntityViewDisplay::load("media.$type_name.default");
    $this->assertInstanceOf(EntityViewDisplay::class, $display);
    $formatter = $display->getComponent($field_name)['type'];
    $this->assertSame('audio_stream', $formatter);

    // Create a media asset and verify that the <audio> tag is present.
    $this->drupalGet("media/add/$type_name");
    $page->fillField('Name', 'Audio stream media asset');
    $page->fillField('URL', 'http://bbcmedia.ic.llnwd.net/stream/bbcmedia_radio2_mf_p');
    $page->pressButton('Save');

    $assert_session->pageTextContains("$type_name Audio stream media asset has been created.");
    $this->clickLink('Audio stream media asset');
    $assert_session->elementExists('css', "audio > source[src='http://bbcmedia.ic.llnwd.net/stream/bbcmedia_radio2_mf_p']");

    // Test an uploaded file with the link (stream) formatter.
    file_put_contents('public://file.mp3', str_repeat('t', 10));
    $file = File::create([
      'uri' => 'public://file.mp3',
      'filename' => 'file.mp3',
    ]);
    $file->save();
    $this->drupalGet("media/add/$type_name");
    $page->fillField('Name', 'Audio stream media asset with local file');
    $page->fillField('URL', \Drupal::service('file_url_generator')->generateAbsoluteString($file->getFileUri()));
    $page->pressButton('Save');
    $assert_session->pageTextContains("$type_name Audio stream media asset with local file has been created.");
    $this->clickLink('Audio stream media asset with local file');
    $audio_element = $assert_session->elementExists('css', "audio > source");
    $public_base_path = \Drupal::request()->getBasePath() . '/' . \Drupal::service('stream_wrapper_manager')->getViaUri('public://')->basePath();
    $this->assertEquals($public_base_path . '/file.mp3', $audio_element->getAttribute('src'));
  }

  /**
   * Check the Video Stream source + formatter functionality.
   */
  public function testVideoStreamTypeCreation() {
    $assert_session = $this->assertSession();
    $page = $this->getSession()->getPage();

    $source_id = 'video_stream';
    $type_name = 'video_stream_type';
    $field_name = 'field_media_' . $source_id;
    $this->doTestCreateMediaType($type_name, $source_id);

    // Check that the source field was created with the correct settings.
    $storage = FieldStorageConfig::load("media.$field_name");
    $this->assertInstanceOf(FieldStorageConfig::class, $storage);
    $field = FieldConfig::load("media.$type_name.$field_name");
    $this->assertInstanceOf(FieldConfig::class, $field);

    // Check that the display holds the correct formatter configuration.
    $display = EntityViewDisplay::load("media.$type_name.default");
    $this->assertInstanceOf(EntityViewDisplay::class, $display);
    $source_field_component = $display->getComponent($field_name);
    $this->assertSame('video_stream', $source_field_component['type']);

    // Create a media asset and verify that the <audio> tag is present.
    $this->drupalGet("media/add/$type_name");
    $page->fillField('Name', 'Video stream media asset');
    $page->fillField('URL', 'http://ecn.channel9.msdn.com/o9/content/smf/smoothcontent/bbbwp7/big%20buck%20bunny.ism');
    $page->pressButton('Save');

    $assert_session->pageTextContains("$type_name Video stream media asset has been created.");
    $this->clickLink('Video stream media asset');
    $assert_session->elementExists('css', "video > source[src='http://ecn.channel9.msdn.com/o9/content/smf/smoothcontent/bbbwp7/big%20buck%20bunny.ism']");

    // Test the video formatter with a remote file (link field).
    $this->drupalGet("media/add/$type_name");
    $page->fillField('Name', 'Video stream media asset with remote file');
    $page->fillField('URL', 'http://techslides.com/demos/sample-videos/small.mp4');
    $page->pressButton('Save');
    $assert_session->pageTextContains("$type_name Video stream media asset with remote file has been created.");
    $this->clickLink('Video stream media asset with remote file');
    $video_element = $assert_session->elementExists('css', 'video');
    // Check some default formatter settings.
    $this->assertEquals('http://techslides.com/demos/sample-videos/small.mp4', $assert_session->elementExists('css', 'source', $video_element)->getAttribute('src'));
    $this->assertEquals('640', $video_element->getAttribute('width'));
    $this->assertEquals('480', $video_element->getAttribute('height'));
    $this->assertNull($video_element->getAttribute('muted'));
    // Modify some formatter settings and check again.
    $source_field_component['settings']['muted'] = TRUE;
    $source_field_component['settings']['width'] = 800;
    $source_field_component['settings']['height'] = 600;
    $display->setComponent($field_name, $source_field_component)->save();
    $this->drupalGet("media/add/$type_name");
    $page->fillField('Name', 'Video stream media asset with remote file 2');
    $page->fillField('URL', 'http://techslides.com/demos/sample-videos/small.mp4');
    $page->pressButton('Save');
    $assert_session->pageTextContains("$type_name Video stream media asset with remote file 2 has been created.");
    $this->clickLink('Video stream media asset with remote file 2');

    $video_element = $assert_session->elementExists('css', 'video');
    $this->assertEquals('800', $video_element->getAttribute('width'));
    $this->assertEquals('600', $video_element->getAttribute('height'));
    $this->assertEquals('muted', $video_element->getAttribute('muted'));
  }

}

<?php

namespace Drupal\media_stream\Plugin\Field\FieldFormatter;

/**
 * Plugin implementation of the 'audio' formatter.
 *
 * @FieldFormatter(
 *   id = "audio_stream",
 *   label = @Translation("Audio Stream"),
 *   description = @Translation("Displays a URL that streams audio content using an HTML5 audio tag."),
 *   field_types = {"uri"}
 * )
 */
class AudioStreamFormatter extends MediaStreamFormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function getMediaType(): string {
    return 'audio';
  }

}

<?php

namespace Drupal\media_stream\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Template\Attribute;
use Drupal\file\Plugin\Field\FieldFormatter\FileMediaFormatterInterface;
use Drupal\media\MediaInterface;

/**
 * Base class for media file formatter.
 */
abstract class MediaStreamFormatterBase extends FormatterBase implements FileMediaFormatterInterface {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    return [
      'controls' => TRUE,
      'autoplay' => FALSE,
      'loop' => FALSE,
      'multiple_file_display_type' => 'tags',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    return [
      'controls' => [
        '#title' => $this->t('Show playback controls'),
        '#type' => 'checkbox',
        '#default_value' => $this->getSetting('controls'),
      ],
      'autoplay' => [
        '#title' => $this->t('Autoplay'),
        '#type' => 'checkbox',
        '#default_value' => $this->getSetting('autoplay'),
      ],
      'loop' => [
        '#title' => $this->t('Loop'),
        '#type' => 'checkbox',
        '#default_value' => $this->getSetting('loop'),
      ],
      'multiple_file_display_type' => [
        '#title' => $this->t('Display of multiple files'),
        '#type' => 'radios',
        '#options' => [
          'tags' => $this->t('Use multiple @tag tags, each with a single source.', ['@tag' => '<' . $this->getHtmlTag() . '>']),
          'sources' => $this->t('Use multiple sources within a single @tag tag.', ['@tag' => '<' . $this->getHtmlTag() . '>']),
        ],
        '#default_value' => $this->getSetting('multiple_file_display_type'),
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    $summary = [];
    $summary[] = $this->t('Playback controls: %controls', ['%controls' => $this->getSetting('controls') ? $this->t('visible') : $this->t('hidden')]);
    $summary[] = $this->t('Autoplay: %autoplay', ['%autoplay' => $this->getSetting('autoplay') ? $this->t('yes') : $this->t('no')]);
    $summary[] = $this->t('Loop: %loop', ['%loop' => $this->getSetting('loop') ? $this->t('yes') : $this->t('no')]);
    switch ($this->getSetting('multiple_file_display_type')) {
      case 'tags':
        $summary[] = $this->t('Multiple file display: Multiple HTML tags');
        break;

      case 'sources':
        $summary[] = $this->t('Multiple file display: One HTML tag with multiple sources');
        break;
    }
    return $summary;
  }

  /**
   * Gets the HTML tag for the formatter.
   *
   * @return string
   *   The HTML tag of this formatter.
   */
  protected function getHtmlTag(): string {
    return static::getMediaType();
  }

  /**
   * Prepare the attributes according to the settings.
   *
   * @param \Drupal\media\MediaInterface $media
   *   A media item.
   * @param string[] $additional_attributes
   *   Additional attributes to be applied to the HTML element. These are
   *   expected to be settings on the formatter, and will be added to the
   *   attributes set if ::getSetting($attribute_name) returns non-empty. In
   *   that case, the attribute name will be used both as key and value. For
   *   example: muted="muted".
   *
   * @return \Drupal\Core\Template\Attribute
   *   Container with all the attributes for the HTML tag.
   */
  protected function prepareAttributes(MediaInterface $media, array $additional_attributes = []): Attribute {
    $attributes = new Attribute();
    foreach (array_merge([
      'controls',
      'autoplay',
      'loop',
    ], $additional_attributes) as $attribute) {
      if ($this->getSetting($attribute)) {
        $attributes->setAttribute($attribute, $attribute);
      }
    }
    return $attributes;
  }

  /**
   * Check if given MIME type applies to the media type of the formatter.
   *
   * @param string $mime_type
   *   The complete MIME type.
   *
   * @return bool
   *   TRUE if the MIME type applies, FALSE otherwise.
   */
  protected static function mimeTypeApplies(string $mime_type): bool {
    [$type] = explode('/', $mime_type, 2);
    return $type === static::getMediaType();
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition): bool {
    // Only expose this formatter to link fields on media entities.
    return $field_definition->getTargetEntityTypeId() === 'media';
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $elements = [];

    $source_files = $this->getSourceFiles($items, $langcode);
    if (empty($source_files)) {
      return $elements;
    }

    $attributes = $this->prepareAttributes($items->getEntity());
    foreach ($source_files as $delta => $files) {
      $elements[$delta] = [
        '#theme' => 'file_' . $this->getHtmlTag(),
        '#attributes' => $attributes,
        '#files' => $files,
      ];
    }
    return $elements;
  }

  /**
   * Gets source files with attributes.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   *   The item list.
   * @param string $langcode
   *   The language code of the referenced entities to display.
   *
   * @return array
   *   Numerically indexed array, which again contains an associative array with
   *   the following key/values:
   *     - file => Remote media URL
   *     - source_attributes => \Drupal\Core\Template\Attribute
   */
  protected function getSourceFiles(FieldItemListInterface $items, string $langcode): array {
    $url_generator = \Drupal::service('file_url_generator');
    $source_files = [];
    foreach ($items as $link) {
      $source_attributes = new Attribute();
      // The use of transformRelative() here has 2 advantages: 1) it prevents
      // problems on multisite set-ups and 2) prevents mixed content
      // errors (http x https).
      $source_attributes
        ->setAttribute('src', $url_generator->transformRelative($link->value));
      if ($this->getSetting('multiple_file_display_type') === 'tags') {
        $source_files[] = [
          [
            'file' => $link->uri,
            'source_attributes' => $source_attributes,
          ],
        ];
      }
      else {
        $source_files[0][] = [
          'file' => $link->uri,
          'source_attributes' => $source_attributes,
        ];
      }
    }
    return $source_files;
  }

}

<?php

namespace Drupal\media_stream\Plugin\Field\FieldFormatter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Template\Attribute;
use Drupal\media\MediaInterface;

/**
 * Plugin implementation of the 'video' formatter.
 *
 * @FieldFormatter(
 *   id = "video_stream",
 *   label = @Translation("Video Stream"),
 *   description = @Translation("Display an URL that streams video content using an HTML5 video tag."),
 *   field_types = {"uri"}
 * )
 */
class VideoStreamFormatter extends MediaStreamFormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function getMediaType(): string {
    return 'video';
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    return [
      'muted' => FALSE,
      'width' => 640,
      'height' => 480,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    return parent::settingsForm($form, $form_state) + [
      'muted' => [
        '#title' => $this->t('Muted'),
        '#type' => 'checkbox',
        '#default_value' => $this->getSetting('muted'),
      ],
      'width' => [
        '#type' => 'number',
        '#title' => $this->t('Width'),
        '#default_value' => $this->getSetting('width'),
        '#size' => 5,
        '#maxlength' => 5,
        '#field_suffix' => $this->t('pixels'),
        '#min' => 1,
      ],
      'height' => [
        '#type' => 'number',
        '#title' => $this->t('Height'),
        '#default_value' => $this->getSetting('height'),
        '#size' => 5,
        '#maxlength' => 5,
        '#field_suffix' => $this->t('pixels'),
        '#min' => 1,
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    $summary = parent::settingsSummary();
    $summary[] = $this->t('Muted: %muted', ['%muted' => $this->getSetting('muted') ? $this->t('yes') : $this->t('no')]);
    $summary[] = $this->t('Width: %width', ['%width' => $this->getSetting('width') ?? '100%']);
    $summary[] = $this->t('Height: %height', ['%height' => $this->getSetting('height') ?? 'auto']);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  protected function prepareAttributes(MediaInterface $media, array $additional_attributes = []): Attribute {
    $attributes = parent::prepareAttributes($media, ['muted']);

    $width = $this->getSetting('width');
    if ($width) {
      $attributes['width'] = $width;
    }
    $height = $this->getSetting('height');
    if ($height) {
      $attributes['height'] = $height;
    }

    // Per HTML specification width and height attributes can only define the
    // video size in pixels. Even though percentage values work well in Google
    // Chrome we use inline styles to set fallback size to comply with the
    // specification.
    if (!$width && !$height) {
      $attributes['style'] = 'width: 100%; height: auto;';
    }

    $poster = $media->getSource()->getMetadata($media, 'poster');
    if ($poster) {
      $attributes->setAttribute('poster', $poster);
    }
    return $attributes;
  }

}

<?php

namespace Drupal\media_stream\Plugin\media\Source;

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\field\FieldConfigInterface;
use Drupal\media\MediaInterface;
use Drupal\media\MediaTypeInterface;
use Drupal\media\Plugin\media\Source\File;

/**
 * Media source wrapping around an audio stream.
 *
 * @MediaSource(
 *   id = "audio_stream",
 *   label = @Translation("Audio stream"),
 *   description = @Translation("Use audio streams for reusable media."),
 *   allowed_field_types = {"uri"},
 *   default_thumbnail_filename = "audio.png"
 * )
 */
class AudioStream extends File {

  /**
   * {@inheritdoc}
   */
  public function getMetadata(MediaInterface $media, $attribute_name) {
    if ($attribute_name === 'default_name') {
      $url = $media->get($this->configuration['source_field'])->value;
      $default_name = basename(parse_url($url, PHP_URL_PATH));
      if ($default_name) {
        return $default_name;
      }
    }
    return parent::getMetadata($media, $attribute_name);
  }

  /**
   * {@inheritdoc}
   */
  public function createSourceField(MediaTypeInterface $type): FieldConfigInterface {
    $storage = $this->getSourceFieldStorage() ?: $this->createSourceFieldStorage();
    return $this->entityTypeManager
      ->getStorage('field_config')
      ->create([
        'field_storage' => $storage,
        'bundle' => $type->id(),
        'label' => $this->t('@label URL', ['@label' => $this->pluginDefinition['label']]),
        'required' => TRUE,
      ]);
  }

  /**
   * {@inheritdoc}
   */
  public function prepareViewDisplay(MediaTypeInterface $type, EntityViewDisplayInterface $display): void {
    $display->setComponent($this->getSourceFieldDefinition($type)->getName(), [
      'type' => 'audio_stream',
      'label' => 'visually_hidden',
    ]);
  }

}

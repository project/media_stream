<?php

namespace Drupal\media_stream\Plugin\media\Source;

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\field\FieldConfigInterface;
use Drupal\file\FileInterface;
use Drupal\image\Entity\ImageStyle;
use Drupal\media\MediaInterface;
use Drupal\media\MediaSourceBase;
use Drupal\media\MediaTypeInterface;

/**
 * Media source wrapping around a video stream.
 *
 * @MediaSource(
 *   id = "video_stream",
 *   label = @Translation("Video stream"),
 *   description = @Translation("Use video streams for reusable media."),
 *   allowed_field_types = {"uri"},
 *   default_thumbnail_filename = "video.png"
 * )
 */
class VideoStream extends MediaSourceBase {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $options = ['' => $this->t('- None -')] + image_style_options(FALSE);
    $form['poster_image_style'] = [
      '#type' => 'select',
      '#title' => $this->t('Poster image style'),
      '#options' => $options,
      '#default_value' => $this->configuration['poster_image_style'],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return parent::defaultConfiguration() + ['poster_image_style' => ''];
  }

  /**
   * {@inheritdoc}
   */
  public function getMetadataAttributes(): array {
    return ['poster' => $this->t('Poster')];
  }

  /**
   * {@inheritdoc}
   */
  public function getMetadata(MediaInterface $media, $attribute_name) {
    switch ($attribute_name) {
      case 'poster':
        $poster = $this->getPosterFile($media);
        return $poster ? $this->buildPosterUrl($poster) : NULL;

      case 'default_name';
        $url = $media->get($this->configuration['source_field'])->value;
        $default_name = basename(parse_url($url, PHP_URL_PATH));
        if ($default_name) {
          return $default_name;
        }
        break;

      case 'thumbnail_uri':
        $poster = $this->getPosterFile($media);
        if ($poster) {
          return $poster->getFileUri();
        }
    }
    return parent::getMetadata($media, $attribute_name);
  }

  /**
   * {@inheritdoc}
   */
  public function createSourceField(MediaTypeInterface $type): FieldConfigInterface {
    $storage = $this->getSourceFieldStorage() ?: $this->createSourceFieldStorage();
    return $this->entityTypeManager
      ->getStorage('field_config')
      ->create([
        'field_storage' => $storage,
        'bundle' => $type->id(),
        'label' => $this->t('@label URL', ['@label' => $this->pluginDefinition['label']]),
        'required' => TRUE,
      ]);
  }

  /**
   * {@inheritdoc}
   */
  public function prepareViewDisplay(MediaTypeInterface $type, EntityViewDisplayInterface $display): void {
    $display->setComponent($this->getSourceFieldDefinition($type)->getName(), [
      'type' => 'video_stream',
      'label' => 'visually_hidden',
    ]);
  }

  /**
   * Returns poster file.
   */
  protected function getPosterFile(MediaInterface $media): ?FileInterface {
    /** @var \Drupal\media\MediaTypeInterface $media_type */
    $media_type = $media->get('bundle')->entity;
    $field_name = $media_type->getFieldMap()['poster'] ?? NULL;
    if ($field_name && $media->hasField($field_name)) {
      /** @var \Drupal\file\Plugin\Field\FieldType\FileFieldItemList $image_field */
      $image_field = $media->get($field_name);
      if ($image_field->isEmpty() || !$image_field instanceof FieldItemListInterface) {
        return NULL;
      }
      return $image_field->referencedEntities()[0];
    }
    return NULL;
  }

  /**
   * Build poster URL.
   */
  protected function buildPosterUrl(FileInterface $poster): ?string {
    $uri = $poster->getFileUri();
    $style = $this->configuration['poster_image_style'];
    // Use path of original image as fallback.
    if (!$style) {
      return $poster->createFileUrl();
    }
    elseif (!$image_style = ImageStyle::load($style)) {
      trigger_error(sprintf('Could not load image style %s.', $style));
      return $poster->createFileUrl();
    }
    if (!$image_style->supportsUri($uri)) {
      trigger_error(sprintf('Could not apply image style %s.', $style));
      return $poster->createFileUrl();
    }
    return \Drupal::service('file_url_generator')
      ->transformRelative($image_style->buildUrl($uri));
  }

}

<?php

namespace Drupal\media_stream\Form;

use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\media\MediaTypeInterface;
use Drupal\media_library\Form\AddFormBase;
use Drupal\media_stream\Plugin\media\Source\AudioStream;
use Drupal\media_stream\Plugin\media\Source\VideoStream;

/**
 * Creates a form to create media entities from stream URLs.
 */
class StreamUrlForm extends AddFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return $this->getBaseFormId() . '_stream';
  }

  /**
   * {@inheritdoc}
   */
  protected function getMediaType(FormStateInterface $form_state): MediaTypeInterface {
    if ($this->mediaType) {
      return $this->mediaType;
    }
    $media_type = parent::getMediaType($form_state);
    $source = $media_type->getSource();
    if (!$source instanceof AudioStream && !$source instanceof VideoStream) {
      throw new \InvalidArgumentException('Can only add media types which use an stream source plugin.');
    }
    return $media_type;
  }

  /**
   * {@inheritdoc}
   */
  protected function buildInputElement(array $form, FormStateInterface $form_state): array {

    // Add a container to group the input elements for styling purposes.
    $form['container'] = [
      '#type' => 'container',
    ];

    $form['container']['url'] = [
      '#type' => 'url',
      '#title' => $this->t(
        'Add @type via URL', ['@type' => $this->getMediaType($form_state)->label()],
      ),
      '#required' => TRUE,
      '#attributes' => ['placeholder' => 'https://'],
    ];

    $form['container']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add'),
      '#button_type' => 'primary',
      '#submit' => ['::addButtonSubmit'],
      '#ajax' => [
        'callback' => '::updateFormCallback',
        'wrapper' => 'media-library-wrapper',
        // Add a fixed URL to post the form since AJAX forms are automatically
        // posted to <current> instead of $form['#action'].
        // @todo Remove when https://www.drupal.org/project/drupal/issues/2504115
        // is fixed.
        'url' => Url::fromRoute('media_library.ui'),
        'options' => [
          'query' => $this->getMediaLibraryState($form_state)->all() + [FormBuilderInterface::AJAX_FORM_REQUEST => TRUE],
        ],
      ],
    ];
    return $form;
  }

  /**
   * Submit handler for the 'Add' button.
   */
  public function addButtonSubmit(array $form, FormStateInterface $form_state): void {
    $this->processInputValues([$form_state->getValue('url')], $form, $form_state);
  }

}
